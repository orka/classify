/*
 * Class Factory Library.
 * Constructs Class Objects to be used for instance creation as well as creating subclasses.
 * Class:
 *     use $class("Name"); to create class factory
 *     Note that Class Facory created will bare the name provided in arguments
 *     Essestial thisngs to remember:
 *         Class Factory will execute a callback (if assigned) when instance iis created using Class Factory Object
 *             "onInstanceCreated(Instance, Class)"
 *              You will have the chance to manage instance before the constructor of the instance is called
 *         Class Factory will execute a callback (if assigned) when a subclass is created using Class Factory Object
 *             "onSubclassCreated(Subclass Factory, Super Class Factory)"
 *             You will have the chance to manage Subclass Factory before any instance is created
 *         In both cases all super class structure will be avoked with the same calls starting from the current Class Factory going all the way up
 *         to the Origin (Base) Class Factory. However returning "false" at any callback will prevent the callback being executed at a higher super chain.
 * Example:
 *     var MyClass = $class('PointClass');
 *     MyClass.onInstanceCreated = function;
 *     MyClass.onSubclassCreated = function;
 *
 *     MyClass.prototype.constructor = function; //constructor methos is called on instance creation
 *
 *     MyClass.super = Base Class Factory
 *     MyClass.superof = Array of all subclasses created using MyClass
 *
 *     var _instance = MyClass();
 *     _instance.class === MyClass; //true
 *     _instance.super === MyClass.super; //true
 *
 *     to evoke super methods use ".call()" method Ex:
 *
 *     _instance.super.constructor.call(_instance);
 *     //same as in a function call
 *     this.super.constructor.call(this);
 *     //same as on Class Factory namespace
 *     MyClass.super.constructor.call(_instance);
 *     //or
 *     MyClass.super.constructor.call(this);
 *
 *     Access to chain of supers ex:
 *     MyClass.super.super.constructor.call(this);
 *
 */
define(['p!-logger'], function($log) {
    'use strict';

    //Injection which is made into Sudo Class (exports) constructor
    //This is done to indicate that a constructor is something developers should not be modifying
    var obfiscate = '\n/*[Sudo Class Constructor/Factory]*/\n\n';
    var GENESUS;
    var MAX_NESTING = 4;
    //constants
    var ATTR_CLASSNAME = 'classname';
    var ATTR_SUPER = 'super';
    var ATTR_CLASS = 'class';
    var ATTR_PROTOTYPE = 'prototype';
    var ATTR_CLASS_LINEAGE = 'lineage';
    //methods
    var METHOD_NATIVE_INSTANCEOF = 'instanceof';
    var METHOD_NATIVE_SUBCLASS = 'subclass';
    var METHOD_NATIVE_MIXIN = 'mixin';
    var METHOD_NATIVE_SUPEROF = 'superof';
    var METHOD_NATIVE_CONSTRUCTOR = 'constructor';
    var METHOD_NATIVE_DECONSTRUCTOR = 'deconstructor';
    var METHOD_NATIVE_INFO = 'deconstructor';
    var METHOD_NATIVE_IS_OFFSPRING_OF_CLASS = 'isOffspring';

    var LISTENER_ON_INSTANCE_CREATED = 'onInstanceCreated';
    var LISTENER_ON_SUBCLASS_CREATED = 'onSubclassCreated';

    //define keys to help prevent mutation on dependant attributes and methods
    var nativeKeys = [
        ATTR_SUPER,
        ATTR_CLASS,
        ATTR_CLASS_LINEAGE,
        METHOD_NATIVE_INSTANCEOF,
        ATTR_CLASSNAME,
        METHOD_NATIVE_MIXIN,
        METHOD_NATIVE_SUBCLASS,
        ATTR_PROTOTYPE,
        METHOD_NATIVE_SUPEROF,
        LISTENER_ON_INSTANCE_CREATED,
        LISTENER_ON_SUBCLASS_CREATED,
        METHOD_NATIVE_DECONSTRUCTOR,
        METHOD_NATIVE_INFO,
        METHOD_NATIVE_CONSTRUCTOR,
        METHOD_NATIVE_IS_OFFSPRING_OF_CLASS
    ];

    /**
     * Illigal characters of Class name
     * @type {Array}
     */
    var classNameIlligals = ['(', ')', '{', '}', '[', ']', ';', '\\', '/', '.', ',', '$', '#'];



    //--------------------------------------------------------   CLASS MODEL



    /**
     * Mixes Object into self at instance
     * unlike mixin on the class all the mixed methods will be bound to the instance
     * @param   {Object} __model  can be Object, Function (Class - will work since the factory inherits constructor prototype)
     * @param   {Array} __ignoreCollection Optional collection of keys or API to ignore
     * @returns {Object}  this instance
     */
    var ClassModel = {

        /**
         * Mizes methods and attributes into current instance only
         * @param   {[type]} __model            [description]
         * @param   {[type]} __ignoreCollection [description]
         * @returns {[type]}                    [description]
         */
        mixin: function(__model, __ignoreCollection) {
            var _key;
            var _collection = __ignoreCollection || [];
            var _source;

            if (!this) {
                throw new Error('You can only mix into the instance! Do not use prototypes method as API!');
            }
            // you can do it this way; Array.isArray(__model)
            switch (Object.prototype.toString.call(__model)) {
                case '[object Array]':
                    throw new Error('You can not use Array as a model!');
                case '[object Null]':
                    throw new Error('You can not use null as a model!');
                case '[object Object]':
                    _source = __model;
                    break;
                default:
                    if (typeof __model === 'function') {
                        _source = __model.prototype;
                    } else {
                        throw new Error('Unable to mix in API to an instance', this);
                    }
            }

            //add native keys to be ignored values
            _collection = _collection !== nativeKeys ? _collection.concat(nativeKeys) : _collection;

            //assign prototype of class - something general???
            for (_key in _source) {
                if (_source.hasOwnProperty(_key) && _collection.indexOf(_key) < 0 && _collection.indexOf(_source[_key]) < 0) {
                    //for the function case - we'd have to bind all methods
                    if (typeof _source[_key] === 'function') {
                        this[_key] = _source[_key].bind(this);
                    } else {
                        this[_key] = _source[_key];
                    }
                } else {
                    // console.error(_key);
                }
            }

            return this;
        },

        /**
         * Type check which is a ssigned to each class instance to be used as a type check
         * @param   {Function} __classFactory Sudo class constructor against which we'll tests the instance type
         * @returns {Boolean}  true if instance is of provided class of any of its subclasses
         */
        instanceof: function(__classFactory) {
            //There are only three case we should check for
            if (typeof __classFactory !== 'function' || typeof __classFactory.constructor !== 'function') {
                //first is arguments itself
                //check a streghtforward case
                return false;
            }

            if (this instanceof __classFactory) {
                //check for the immediate match of a provided class
                return true;
            }
            //itterate over subclasses of a provided class to wee if any of subclasses is the constructor
            return instanceofChildren(this, __classFactory);
        },

        info: function() {
            $log('Class Name: ' + this[ATTR_CLASSNAME] + '\n' + 'super: ' + this[ATTR_SUPER]);
        }
    };



    //--------------------------------------------------   EVALUATION



    /**
     * Evaluates arguments when constructing a class
     * @param   {Closure} __this Object
     * @param   {String} __name Class Name
     * @param   {Object} __api  Class API - model
     * @returns {String}  error message
     */
    function evaluateClassCreationArguments(__this, __name, __api) {
        var _error = '';
        //if "this" is defined - then someone used keyword "new" which will break the chain of prototype
        // if (__this) {
        //     return 'Illegal invocation! Do not use keyword "new" when evoking a new instance or class! Use [ClassName]( arg,... )';
        // }
        //evaluate name - get error message if fails
        _error = evaluateClassName(__name);

        if (_error !== '') {
            return _error;
        }
        //evaluate api - get error message if fails
        _error = evaluateClassAPI(__api, __name);

        if (_error !== '') {
            return _error;
        }
        //return empty string if no error occurred validating arguments
        return _error;
    }

    /**
     * Evaluates name of class
     * Prevent unwanted characters that may be used to inject script
     * Throws error if doesn't pass the validation
     * @param   {String} __name name of class
     * @returns {String} Error Message
     */
    function evaluateClassName(__name) {
        var _i, _char;

        if (typeof __name !== 'string') {
            return 'Class Name is not a string';
        }

        if (__name.trim() === '') {
            return 'Class Name is an empty String';
        }

        if (__name.length > 40) {
            return 'Class Name is too large. 40 chars MAX!';
        }

        //prevent characters that can inject script
        //not really needed - but may be useful way, way in the future
        for (_i = 0; _i < __name.length; _i += 1) {
            _char = __name[_i];
            if (classNameIlligals.indexOf(_char) >= 0) {
                return 'Class name contains illigal characters';
            }
        }

        if (__name.charAt(0) !== __name.charAt(0).toUpperCase()) {
            return 'Class Name must start with an upper case letter';
        }

        return '';
    }

    /**
     * Evaluates Class API Object
     * Some of the methods must be defined to be sutable for Class
     * @param   {Object} __api  Class API
     * @param   {String} __name Class Name for Error messages
     * @returns {String}  Error message
     */
    function evaluateClassAPI(__api, __name) {
        //Object.prototype.toString.call( __model ) === '[object Null]'
        if (Object.prototype.toString.call(__api) !== '[object Object]') {
            return 'Provided API is not an object. New Class "' + __name + '" must have API defined';
        }
        //if constructor has not been defined or is not a function
        if (!__api.hasOwnProperty('constructor') || typeof __api.constructor !== 'function') {
            return 'Provided API for Class "' + __name + '" is missing constructor() method';
        }

        if (!__api.hasOwnProperty('deconstructor') || typeof __api.deconstructor !== 'function') {
            return 'Provided API for Class "' + __name + '" is missing deconstructor() method';
        }
        //
        return '';
    }

    /**
     * Mixing prototypes, but omitting native keys of class factories
     * Ables to take a Object, Function, Class, Instance to be mixed into Class
     * @param   {Object} __classConstructor  Class COnstructor
     * @param {Object} __modelObject the object or function to mix into the passed in constructor
     * @returns {Object} The mixed class
     */
    function mixinToClass(__classConstructor, __modelObject) {
        var _object, _key;

        if (!__classConstructor) {
            throw 'Unable to identify Constructor!';
        }

        if (typeof __modelObject === 'undefined') {
            throw 'No argument provided!';
        }

        switch (Object.prototype.toString.call(__modelObject)) {
            case '[object Object]':
                _object = __modelObject;
                break;
            case '[object Function]':
                _object = __modelObject.prototype;
                break;
            case '[object Array]':
                throw 'Unable to mixin Array to a class constructor';
            default:
                throw 'Unable to mixin API to a class constructor';
        }

        //mix all attributes and methods of model into classs
        for (_key in _object) {
            //except the native attr and methods
            if (_object.hasOwnProperty(_key) && nativeKeys.indexOf(_key) < 0) {
                __classConstructor.prototype[_key] = _object[_key];
            } else {
                // console.error(_key);
            }
        }

        return __classConstructor;
    }

    /**
     * Creates contructor function of a name provided
     * This is done to achieve effect of constructor function name being a custom to developers needs
     * This solve a lot of debug inconviniances
     * @param   {String} __name Name of Class
     * @returns {Function}  Class contructor
     */
    function createConstructor(__name) {
        //handle for the function constructor
        var _classConstructor;
        //create a dynamic constructor
        eval('_classConstructor = function ' + __name + '() {' + obfiscate + '};');
        //new function
        return _classConstructor;
    }

    /**
     * Mixes object properties
     * @param   {Object} __modelObject properties to inherit from
     * @param   {Object} __targetObject new propertied will be assigned to this object
     * @returns {Object}   Mixed object
     */
    function applyModelAPI(__modelObject, __targetObject) {
        var _key;
        //assign prototype of class - something general???
        for (_key in __modelObject) {
            if (__modelObject.hasOwnProperty(_key)) {
                __targetObject[_key] = __modelObject[_key];
            } else {
                // console.error(_key);
            }
        }

        return __targetObject;
    }

    /**
     * Creates super chain of subclass
     * Ittarates over chain of all parent classes constructing object which is the super chain
     * @param   {Function} __parentExports Sudo Class constructor which may host its own chain of supers
     * @param   {Funciton} __classFactory  Sudo Class constructor to which new chain of supers are assigned
     * @returns {Function}  __classFactory
     */
    function assignSuperStructure(__parentExports, __classFactory) {
        //current super as we'll be itterating over entire chain and assigning every new class as _super
        var _super, _currentSuper, _enclosedSuperStructure, _assignTo, _lineage = [];
        //assign super object to be a superclass (Sudo)
        __classFactory[ATTR_SUPER] = __parentExports;

        //assign a starting super class to be the first in chain of supers
        _super = __parentExports;

        while (_super) {
            if (_currentSuper) {
                _assignTo = _currentSuper;
            }
            _currentSuper = _super;
            //start closure chain
            _enclosedSuperStructure = _enclosedSuperStructure || _currentSuper;

            // if (_super !== _currentSuper) {
            //mix proto Class to enclosure binding it with instance
            applyModelAPI(_super.prototype, _currentSuper);
            //ensure to cover speacial case of constructors !!!
            _currentSuper.constructor = _super.prototype.constructor; //.bind(__instane);
            // }
            //go up the chain of supers
            _super = _super[ATTR_SUPER];
            //ufter first run build the super chain that will be a part of "_enclosedSuperStructure"
            if (_assignTo) {
                _assignTo[ATTR_SUPER] = _currentSuper;
            }

            _lineage.push(_currentSuper);
        }
        //finally assign the super chain
        __classFactory[ATTR_SUPER] = _enclosedSuperStructure;
        __classFactory[ATTR_CLASS_LINEAGE] = _lineage;
        // __parentExports[ATTR_CLASS_LINEAGE] = __parentExports[ATTR_CLASS_LINEAGE] || [];
        //return target class
        return __classFactory;
    }
    /**
     * Measures chain of superclasses length
     * @param   {[type]} __class [description]
     * @returns {[type]}         [description]
     */
    function getSubclassNestingDepth(__class) {
        var _nestingLevel = 0;
        var _superClass = __class[ATTR_SUPER];

        while (_superClass) {
            _superClass = _superClass[ATTR_SUPER];
            _nestingLevel += 1;
        }

        return _nestingLevel;
    }

    /**
     * A reverse lookup of inctance relation to a given class
     * @param   {Object} __classFactory Class Factory
     * @param   {Object} __instance instance of class Factory (anything really)
     * @returns {Boolean} true if class has relations to a given instance
     */
    function isOffspringOfClass(__classFactory, __instance) {
        return __instance && __instance.instanceof && __instance.instanceof(__classFactory);
    }

    /**
     * Ittarates over all the subclasses created against provided class accessing "superof" collection object
     * and checks the type of instance
     * @param   {Object} __instance     Class instance
     * @param   {Function} __classFactory Sudo Class constructor
     * @returns {Boolean} true if instance is of provided class of any of its subclasses
     */
    function instanceofChildren(__instance, __classFactory) {
        var _i, _childClass, _subclassesCollection;

        if (!__classFactory) {
            return false;
        }

        if (typeof __classFactory !== 'function') {
            return false;
        }

        _subclassesCollection = __classFactory[METHOD_NATIVE_SUPEROF];
        //typeof __classFactory !== 'function'
        //if a collection of subclasses not found
        if (Object.prototype.toString.call(_subclassesCollection) !== '[object Array]') {
            return false;
        }

        //looping over the collection of subclasses testing type
        for (_i = 0; _i < _subclassesCollection.length; _i += 1) {
            //get the class the arguments is a superclass of
            _childClass = _subclassesCollection[_i];
            //silently skip the value
            if (typeof _childClass !== 'function') {
                continue;
            }
            //we have identified constructor and use a native method to verify
            if (typeof _childClass.constructor === 'function' && __instance instanceof _childClass.constructor) {
                return true;
            }

            if (instanceofChildren(__instance, _childClass) === true) {
                return true;
            }
            // return instanceofChildren(__instance, _childClass);
        }
        //default return of no match was found
        return false;
    }

    //We do use execOnInstanceCreatedListeners in the eval scripts
    /*eslint-disable no-unused-vars*/
    /**
     * Executes method "onInstanceCreated()" of a class and all its superclasses
     * This is to give developer ability to track instances of all subclasses individually
     * The order of execution is from class to last super
     * if "onInstanceCreated()" returns flase - the ietteration stops
     * @param   {Function} __classFactory immadiate class constructor of instance
     * @param   {Object} __instance instance
     */
    function execOnInstanceCreatedListeners(__classFactory, __instance) {
        var _handler;
        var _class = __classFactory;
        //collect classes
        while (_class) {
            _handler = _class[LISTENER_ON_INSTANCE_CREATED];
            //call method providing instance and class cunstructor
            if (typeof _handler === 'function' && _handler(__instance, __classFactory) === false) {
                break;
            }
            //next class up the chain of supers
            _class = _class[ATTR_SUPER];
        }
    }
    /*eslint-enable no-unused-vars*/

    /**
     * Simmilar to "execOnInstanceCreatedListeners"
     * Executes method "onSubclassCreated()" of a class and all its superclasses
     * This is to give developer ability to track subclass of all superclassed individually
     * The order of execution is from class to last super
     * if "onSubclassCreated()" returns flase - the ietteration stops
     * @param   {Function} __classFactory immadiate class constructor of instance
     * @param   {Object} __subclass instance
     */
    function execOnSubclassCreatedListeners(__classFactory, __subclass) {
        var _handler;
        var _class = __classFactory;
        //collect classes
        while (_class) {
            _handler = _class[LISTENER_ON_SUBCLASS_CREATED];
            //call method providing subclass and class cunstructor
            if (typeof _handler === 'function' && _handler(__subclass, __classFactory) === false) {
                break;
            }
            //next class up the chain of supers
            _class = _class[ATTR_SUPER];
        }
    }

    /**
     * A subclass constructor
     * Creates a class exporter of the same name as requested in arguments
     * A class exporter is NOT the same cunstructor of class itself
     * @param   {Function} __superClass Sudo Class constructor (super) is passed supers chain will be assigned
     * @param   {String} __name  name of class
     * @param {Object} __api model of the class structure << required methods: constructor(), deconstructor() >>
     * @returns {Function}   new Sudo Class constructor of the same name
     */
    function classConstructor(__superClass, __name, __api) {
        var _classConstructor, _classFactory, _error;

        //acquire error message if aprguments failed validation
        _error = evaluateClassCreationArguments(this, __name, __api);

        //if evaluation returned non empty string - throw Error
        if (_error !== '') {
            throw new Error(_error);
        }

        //create Custom Name Funciton (Class Object)
        _classConstructor = createConstructor(__name);
        // __name = 'Class_' + __name;
        //create constructor block
        _classFactory = [
            '_classFactory = function ' + __name + '() {',
            //this is to push the function block into invisible area
            obfiscate,
            //prevent using new on _classFactory
            // '\tif (this) {',
            // '\t\tthrow new Error("Illegal invocation! Do not use keyword (new) when evoking a new instance or class!");',
            // '\t};',

            //create instance
            '\tvar _instance = new _classConstructor(arguments);',

            //assign super chain
            '\t_instance[ATTR_SUPER] = _classFactory[ATTR_SUPER];',

            //assign own Class
            '\t_instance[ATTR_CLASS] = _classFactory;',

            //add original name of class and options
            '\t_instance[ATTR_CLASSNAME] = __name;',

            //add a subclass super generator only if it is a subclass
            '\texecOnInstanceCreatedListeners(_classFactory, _instance);',

            //immediatly call "constructor()" method of a new instance
            '\t_instance.constructor.apply(_instance, arguments);',

            //return new instance
            '\treturn _instance;',
            '};'
        ].join('\n');

        //create sudo constructor
        eval(_classFactory);

        _classFactory.api = __api;

        //hanlde case of a Class contruction as suposed to a subclass
        if (!__superClass) {
            //assign prototype of class - somthing general???
            applyModelAPI(ClassModel, _classConstructor.prototype);
            _classFactory[METHOD_NATIVE_SUPEROF] = [];
            _classFactory[ATTR_CLASS_LINEAGE] = [];
            _classFactory[ATTR_SUPER] = undefined;
        } else {
            //return error if maximu depth exceeded
            //Great to keep a tab on nesting depths
            if (getSubclassNestingDepth(__superClass) > MAX_NESTING) {
                throw new Error('Maximum nesting level of "' + MAX_NESTING + '" exceeded!');
            }
            //mix supers methods to constructor prototype
            applyModelAPI(__superClass.prototype, _classConstructor.prototype);
            //after adding super create a super structure
            assignSuperStructure(__superClass, _classFactory);
            //notify supers about class being constructed
            execOnSubclassCreatedListeners(__superClass, _classFactory);
            try {
                //asign breadcrumbs
                __superClass[METHOD_NATIVE_SUPEROF] = __superClass[METHOD_NATIVE_SUPEROF] || [];
                //add child classes to a superclass
                __superClass[METHOD_NATIVE_SUPEROF].push(_classFactory);
            } catch (__err) {
                throw 'Class: "' + __superClass.classname + '"" - appears to be frozen! Unfreeze!!!';
            }
        }
        //now apply API to a new Class
        applyModelAPI(__api, _classConstructor.prototype);
        //assign prototype to proxy Sudo Class Constructor
        _classFactory.prototype = _classConstructor.prototype;
        //Totally strange but there is no need at all to expose SudoClass constructor
        _classFactory.constructor = _classConstructor;
        //not manditory but usefull
        _classFactory[ATTR_CLASSNAME] = __name;
        // add Class API to create a subclass
        _classFactory[METHOD_NATIVE_SUBCLASS] = classConstructor.bind(null, _classFactory);
        // add Class API to mix in new object API
        _classFactory[METHOD_NATIVE_MIXIN] = mixinToClass.bind(null, _classFactory);
        //instance check on class object
        _classFactory[METHOD_NATIVE_IS_OFFSPRING_OF_CLASS] = isOffspringOfClass.bind(null, _classFactory);

        //    __superClass[METHOD_NATIVE_IS_SUPEROF] = isClassSuperOfSubclass.bind(null, __superClass);
        return _classFactory;
    }

    //Bind nothing for the Genesus node
    GENESUS = classConstructor.bind(null, null);

    /**
     * Add extra property to genesus
     * @param   {Object}  __object identifyer object
     * @returns {Boolean}     true if provided object is a class of GENESUS
     */
    GENESUS.isClass = function(__object) {
        var _constr;
        var _i;

        if (!__object || !__object.constructor) {
            return false;
        }

        _i = 0;

        _constr = __object.constructor;

        while (_constr) {
            if (_constr.constructor === classConstructor.constructor) {
                return true;
            }

            _constr = _constr.constructor;
            _i += 1;
        }
        return false;
    };

    return Object.freeze(GENESUS);
});